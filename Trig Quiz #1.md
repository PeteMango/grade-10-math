# Trigonometry Test #1
## Question 1

$`\because \angle B^\prime = \angle B \text{ and } \angle C^\prime = \angle C`$

$`\therefore \triangle AB^\prime C^\prime \sim \triangle ABC  \quad (\text{Angle Angle Similarity}) `$

$`\therefore \dfrac{AB^\prime}{B^\prime C^\prime} = \dfrac{AB}{BC} `$

$`\therefore \dfrac{30}{14} = \dfrac{30+x}{22}`$

$`14(30+x) = 22 \times 30`$

$`420 + 14x = 660`$

$`14x = 240`$

$`x = 17.1428571 \approx 17.14 `$

$`\therefore \dfrac{AC^\prime}{B^\prime C^\prime} = \dfrac{AC}{BC} `$

$`\therefore \dfrac{y}{14} = \dfrac{y+15}{22} `$

$`22y = 14(y + 15)`$

$`22y = 14y + 210`$

$`8y = 210`$

$`y = 26.25`$

`x = 17.14 and y = 26.25`

## Question 2
$`\text{Draw a line perpendicular line intersecting BC at H and let AH be the height of the triangle with base BC}`$

$`h = b \sin A`$

$`h = 11.3 \sin (32)`$

$`h \approx 5.99`$

$`\because h (5.99) \lt 6.8 \lt 11.3`$

$`\therefore 2 \triangle 's \text{ exist}`$

$`\text{ Lets call point } T  \text{ is the height that is perpendicular on side } AB \text{ and connects to point } C. \text { and } B^\prime \text{ be the other possible point of } B.`$

-------------------------
$`\text{ Case } 1:`$

$`\angle CB^\prime T = \sin^{-1} \Bigl(\dfrac{5.99}{6.8} \Bigr)`$

$`\angle CB^\prime T = 61.75^o`$

$`\angle AB^\prime C = 180 - 61.75 = 118.25^o`$

$`\angle ACB^\prime = 180 - 118.25 - 32 = 29.75^o`$

$`\dfrac{AB}{\sin \angle ACB^\prime} = \dfrac{CB^\prime}{\sin A}`$

$`\dfrac{AB}{\sin29.75} = \dfrac{6.8}{\sin 32}`$

$`AB = \dfrac{\sin 29.75 \times 6.8}{\sin32}`$

$`AB = 6.37`$

-------------------------
$`\text{ Case } 2: `$

$`\angle ABC = \angle CB^\prime T = 61.75^o`$

$`\angle ACB = 180 - 32 - 61.75 = 86.25^o`$

$`\dfrac{AB}{\sin C} = \dfrac{CB}{\sin A}`$

$`\dfrac{AB}{\sin 86.25} = \dfrac{6.8}{\sin 32}`$

$`AB = \dfrac{\sin 86.25 \times 6.8}{\sin 32}`$

$`AB = 12.8`$

--------------------------

`AB could either have a length of 6.37cm or 12.8cm.`

## Question 3

$`\text{let the square be } \square ABCD \text{ and the inner triangle be } \triangle AEF \text{ with E being a point on BC and F being a point on CD}`$

$`\sin (\beta) = \dfrac{EF}{AE} = \dfrac{EF}{1} = EF`$

$`\sin (\alpha) \sin(\beta) = \dfrac{EC}{EF} \times \dfrac{EF}{AE} = \dfrac{EC}{AE} = \dfrac{EC}{1} = EC`$

$`\cos(\alpha) \sin(\beta) = \dfrac{CF}{EF} \times \dfrac{EF}{AE} = \dfrac{CF}{AE} = \dfrac{CF}{1} = CF`$

$`\text{Draw a parallel line to } CD \text{ that connects point } E \text{ to } AD. \quad \sin(\alpha + \beta) = \dfrac{CD}{AE} = \dfrac{CD}{1} = CD`$

$`\cos(\alpha) \cos(\beta) = \dfrac{AD}{AF} \times \dfrac{AF}{AE} = \dfrac{AD}{1} = AD`$

$`\sin(\alpha) \cos(\beta) = \dfrac{FD}{AF} \times \dfrac{AF}{AE} = \dfrac{FD}{1} = FD`$

$`\cos(\beta) = \dfrac{AF}{AE} = \dfrac{AF}{1} = AF`$

$`\text{Draw a parallel line to } CD \text{ that connects point } E \text{ to } AD. \quad \cos(\alpha + \beta) = \dfrac{BE}{AE} = \dfrac{BE}{1} = BE`$

$`\therefore \sin(\alpha + \beta) = CD = CF + FD = \cos(\alpha) \sin(\beta) + \sin(\alpha) \cos(\beta)`$

$`\therefore \cos(\alpha + \beta) = BE = \cos(\alpha - \beta)`$

`sin(a + b) = cos(a)sin(b) + sin(a)cos(b)` <br/>
`cos(a + b) = cos(a - b)`
